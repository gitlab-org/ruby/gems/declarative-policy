unreleased:

- Drop explicit support for Ruby 2.6 and 2.7 by removing those versions from
  the CI matrix. These Ruby versions are now past EOL.
- Rename default condition scope name to `:user_and_subject`
- Clarify the use of `User` and `Subject` in README and documentation

1.1.1:

- Define development dependencies

1.1.0:

- Add cache invalidation API: `DeclarativePolicy.invalidate(cache, keys)`
- Include actor class name in cache key

1.0.1:

- Added unit level tests for `lib/declarative_policy/rule.rb`
